# PeerTube-desktop

PeerTube-Desktop is a native (GTK3) peertube client, developed for GNU/Linux but with portability in mind[^1].
It is free software, under the GPLv3 (or later) license.

## Screenshots
<img src="/screenshots/1.png" alt="PeerTube-Desktop window when the width is fairly large" width="500"/>
<img src="/screenshots/2.png" alt="PeerTube-Desktop window when the width is fairly small" height="500"/>
<img src="/screenshots/5.png" alt="PeerTube-Desktop window when the width is of medium size" width="500"/>
<img src="/screenshots/6.png" alt="PeerTube-Desktop window when watching a video" width="500"/>

A short video showing how PeerTube-Desktop is responsive: (note: the flickering is an artifact from the recording)
![PeerTube-Desktop in action](screenshots/output.mp4)


## Installation instructions

Clone this repository, `cd` into it, then:

```sh
meson _build
ninja -C _build
sudo ninja -C _build install
```

The `requests_futures` python package is required to run the program.  
We want to drop this requirement and replace it with GTK's native libsoup.  
In the meantime, to install it do
```sh
sudo pip install requests_futures
```

Then you can execute peertube-desktop to open the client.

Instead of installing it, you can also open the project in gnome-builder and click "Run".  
There is also a flatpak configuration present in the repo, if you prefer that.

## Roadmap

### Reach feature parity with web client

https://framagit.org/framasoft/peertube/PeerTube

* [x] Home page
* [ ] Search
* [ ] Account actions (subscribe/comment/etc): as an added functionality,
  should work with any ActivityPub compliant account on any server (Mastodon,
  GnuSocial, Pleroma, etc), with priority on PeerTube and Mastodon accounts.
* [ ] Accessibility


### Download *via* WebTorrent and Bittorrent and seed

At this stage we only use the http endpoint of the videos, without having to
think about p2p too much. But it would be very nice to be able to contribute back.

For that, we should aim to implement WebTorrent, which gives feature-parity
with web client, and BitTorrent.

We could use libtorrent (also used by Deluge): they seem to be considering WebTorrent
integration: https://github.com/arvidn/libtorrent/issues/223 and might have
this working soon™ (hopefully by the time we get to this stage)


### Browser integration

In tandem with a browser extension that rewrites links on click (see
https://github.com/FreeTubeApp/freetube-redirect), listen to the `peertube://`
url scheme and open videos from the browser in a native application.

This needs to get links from a list of peertube instances + user supplied
links, but we might also want this to work with sites like YouTube or other
sites supported by youtube-dl

Make sure an instance can be added manually to the extension



[^1]: don't hesitate to get in touch if you want to package or port it!
