from gi.repository import Gtk

@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/about_dialog.ui')
class AboutDialog(Gtk.AboutDialog):
    """About dialog"""

    __gtype_name__ = 'AboutDialog'

    def __init__(self):
        super().__init__()

    def _about_response(self, widget, data=None):
        widget.destroy()
