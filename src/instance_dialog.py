#
# Copyright © 2019  Matthieu De Beule <matthieu.de@beule.be>
# Copyright © 2019  Antoine Fontaine <antoine.fontaine@epfl.ch>
#
# This file is part of PeerTube-Desktop.
#
# PeerTube-Desktop is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# PeerTube-Desktop is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# PeerTube-Desktop.  If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Handy, Gtk, GLib

@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/instance_dialog.ui')
class InstanceDialog(Handy.Dialog):
    """Instance choosing dialog"""

    __gtype_name__ = 'InstanceDialog'

    entry = Gtk.Template.Child()

    def __init__(self, parent, **kwargs):
        kwargs.update({"use-header-bar": 1})
        super().__init__(**kwargs)
        self.set_transient_for(parent)
        self.set_default_response(-10)
