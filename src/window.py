#
# Copyright © 2019  Matthieu De Beule <matthieu.de@beule.be>
# Copyright © 2019  Antoine Fontaine <antoine.fontaine@epfl.ch>
#
# This file is part of PeerTube-Desktop.
#
# PeerTube-Desktop is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# PeerTube-Desktop is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# PeerTube-Desktop.  If not, see <https://www.gnu.org/licenses/>.

import sys, os
from gi.repository import Gtk, GLib, Gio, Gst, Handy, GObject
from gi.repository import GstVideo
from .video_element import VideoElement
from .instance_dialog import InstanceDialog
from .about_dialog import AboutDialog
from .video_list import VideoList
from .watch_video import WatchVideo
from requests_futures.sessions import FuturesSession
Gst.init(None)



@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/window.ui')
class PeertubeDesktopWindow(Gtk.ApplicationWindow):
    """Main window of the PeerTube-Desktop application"""
    __gtype_name__ = 'PeertubeDesktopWindow'

    scroll_stack = Gtk.Template.Child()
    video_list_stack = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    switcher_bar = Gtk.Template.Child()
    title_label = Gtk.Template.Child()
    squeezer = Gtk.Template.Child()
    menu_button = Gtk.Template.Child()

    instance_views = [
            { "title": "Recently Added", "icon": "appointment-new-symbolic", "sort_method": "publishedAt" },
            { "title": "Most Viewed", "icon": "face-devilish-symbolic", "sort_method": "views" },
            { "title": "Trending", "icon": "starred-symbolic", "sort_method": "trending" },
        ]

    session = FuturesSession()


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = Gio.Settings.new("org.peertube.PeerTube-Desktop")
        self.scroll_stack.set_visible_child_name("loading")
        self.watch_video = WatchVideo()
        self.scroll_stack.add_named(self.watch_video, "watch_video")

        # display the right HdyViewSwitcher on start
        self.on_child_changed(self.squeezer)

        instance_url = self.settings.get_string("instance-url")
        chosen_view = "views"
        def loaded_cb(sort_method):
            if sort_method == chosen_view:
                self.scroll_stack.set_visible_child_name("video_list_stack")
        for instance in self.instance_views:
            vid_list = VideoList(
                sort_method   = instance["sort_method"],
                instance_url  = instance_url,
                window        = self,
                loaded_cb     = loaded_cb,
            )
            self.video_list_stack.add_titled(vid_list, instance["sort_method"], instance["title"])
            self.video_list_stack.child_set_property(vid_list, "icon-name", instance["icon"])
        if(not instance_url):
            self.change_instance()

    @Gtk.Template.Callback()
    def on_child_changed(self, widget, data=None):
        """React to changes to make the stack chooser be responsive"""
        self.switcher_bar.set_property("reveal",
                        self.squeezer.get_visible_child() == self.title_label)

    @Gtk.Template.Callback()
    def on_destroy(self, widget, data=None):
        #Remove queued requests from the session
        self.session.__exit__()

    def create_watch_video(self, video, title, date_and_views, channel, account):
        """Open a video"""
        self.scroll_stack.set_visible_child_name("watch_video")
        self.back_button.show()
        self.squeezer.hide()
        self.menu_button.hide()
        self.switcher_bar.hide()
        self.watch_video.play_video(video, title, date_and_views, channel, account)

    @Gtk.Template.Callback()
    def on_back_button_clicked(self, widget, data=None):
        """Close the video and return to main screen"""
        self.scroll_stack.set_visible_child_name("video_list_stack")
        self.back_button.hide()
        self.menu_button.show()
        self.squeezer.show()
        self.switcher_bar.show()
        self.watch_video.close_view()

    @Gtk.Template.Callback()
    def change_instance(self, widget=None, data=None):
        """Switch to instance specified by the input in a dialog"""
        dialog = InstanceDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.APPLY:
           self.scroll_stack.set_visible_child_name("loading")
           self.settings.set_string("instance-url", dialog.entry.get_text())
           for instance in self.instance_views:
               current_list = self.video_list_stack.get_child_by_name(instance["sort_method"])
               current_list.instance_url = dialog.entry.get_text()
               current_list.request_videos()
        dialog.destroy()

    @Gtk.Template.Callback()
    def about_dialog(self, widget, data=None):
        """Open "About" dialog"""
        about = AboutDialog()
        about.run()
        about.destroy()
