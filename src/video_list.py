#
# Copyright © 2019  Matthieu De Beule <matthieu.de@beule.be>
# Copyright © 2019  Antoine Fontaine <antoine.fontaine@epfl.ch>
#
# This file is part of PeerTube-Desktop.
#
# PeerTube-Desktop is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# PeerTube-Desktop is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# PeerTube-Desktop.  If not, see <https://www.gnu.org/licenses/>.

import sys, os
from gi.repository import Gtk, GLib, Gio, Gst, Handy
from gi.repository import GstVideo
from .video_element import VideoElement
from .instance_dialog import InstanceDialog
from .watch_video import WatchVideo
from requests_futures.sessions import FuturesSession
Gst.init(None)



@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/video_list.ui')
class VideoList(Gtk.ScrolledWindow):
    """A list of videos, in a scrolling view"""
    __gtype_name__ = 'PeertubeDesktopVideoList'

    flowbox = Gtk.Template.Child()

    session = FuturesSession()


    def __init__(self, **kwargs):
        self.sort_method   = kwargs.pop("sort_method")
        self.instance_url  = kwargs.pop("instance_url")
        self.loaded_cb     = kwargs.pop("loaded_cb")
        self.window        = kwargs.pop("window")
        super().__init__(**kwargs)

        self.batches_loaded = 1
        self.request_videos()

    def request_videos(self, start=0):
        """Get a batch of videos and add them to the list"""
        self.session.get(self.instance_url + "/api/v1/videos/?sort=-" + self.sort_method,
                hooks={'response': self.get_videos},params={"count": 200, "start": start*200})

    def get_videos(self, response, *args, **kwargs):
        """Interpret response and add the videos to the list"""
        #idle_add is necessary because Gtk isn't thread-safe
        #Every call to Gtk must be done in the main thread, which is what idle_add does

        # sort method is used to identify which is which
        GLib.idle_add(self.loaded_cb, self.sort_method)

        videos = response.json().get("data")
        GLib.idle_add(self.add_videos, videos)

    def add_videos(self, videos):
        """Add videos to the list"""
        if(self.batches_loaded == 1):
            self.flowbox.foreach(lambda element: self.flowbox.remove(element))
        for video in videos:
            new_element = VideoElement(self.window, video, self.session, self.instance_url)
            self.flowbox.add(new_element)
        self.load_more_button = Gtk.Button.new_with_label("Load More")
        self.load_more_button.connect("clicked", self.more_videos)
        self.flowbox.add(self.load_more_button)
        self.load_more_button.show()

    def more_videos(self, widget, data=None):
        """Add more videos to an already populated list"""
        self.flowbox.remove(self.load_more_button.get_parent())
        self.request_videos(start=self.batches_loaded)
        self.batches_loaded += 1
