#
# Copyright © 2019  Matthieu De Beule <matthieu.de@beule.be>
# Copyright © 2019  Antoine Fontaine <antoine.fontaine@epfl.ch>
#
# This file is part of PeerTube-Desktop.
#
# PeerTube-Desktop is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# PeerTube-Desktop is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# PeerTube-Desktop.  If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gst, Gtk, GLib, GstVideo

@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/watch_video.ui')
class WatchVideo(Gtk.Grid):
    """Contains the video and associated elements like a description and
    comments section"""
    __gtype_name__ = "watch_video"

    video_box = Gtk.Template.Child()
    title = Gtk.Template.Child()
    date_and_views = Gtk.Template.Child()
    channel = Gtk.Template.Child()
    account = Gtk.Template.Child()
    progress = Gtk.Template.Child()
    volume_slider = Gtk.Template.Child()
    play_button = Gtk.Template.Child()

    def __init__(self):
        self.is_playing = False
        super(WatchVideo, self).__init__()
        self.video_uuid = None
        self.saved_state = Gst.State.NULL

        self.slider_handler_id = self.progress.connect("value-changed", self.on_slider_seek)


        self.player = Gst.ElementFactory.make("playbin", "player")
        self.mult = 1
        self.duration = 60
        self.volume_slider.set_value(self.player.get_property("volume"))
        #FIXME use gtkglsink instead (gtksink has NO hardware acceleration!)
        videosink = Gst.ElementFactory.make("gtksink")
        self.player.set_property("video-sink", videosink)
        #self.player.set_property("force-aspect-ratio", True)
        videowidget = videosink.get_property("widget")
        self.video_box.add(videowidget)
        self.video_box.show_all()
        bus = self.player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", self.on_message)

    def play_video(self, video, title, date_and_views, channel,
                    account):
        """Start playing a video"""
        uuid = video.get("uuid")
        self.play_button.set_active(False)
        if(self.video_uuid != uuid):
            #if not the same video as before (or first) initialize from scratch
            self.player.set_property("uri", video.get("files")[0].get("fileDownloadUrl"))
            self.play()
            self.title.set_property("label", title)
            self.date_and_views.set_property("label", date_and_views)
            self.channel.set_property("label", channel)
            self.account.set_property("label", account)
            #TODO set description etc, load comments,...
        elif(uuid == self.video_uuid):
            #if this video is the same as before, restore to previous video state
            self.player.set_state(self.saved_state)
            #TODO set time of video back to previous time (maybe -5 seconds or so?)

        self.video_uuid = uuid

    def play(self):
        """Unpause (or start) the video"""
        self.is_playing = True
        self.player.set_state(Gst.State.PLAYING)
        # starting up a timer to check on the current playback value
        GLib.timeout_add(1000, self.update_slider)

    def pause(self):
        """Pause the video"""
        self.is_playing = False
        self.player.set_state(Gst.State.PAUSED)

    def close_view(self):
        """Close the video and set the state in accordance with that"""
        self.saved_state = self.player.get_state(Gst.CLOCK_TIME_NONE).state
        self.player.set_state(Gst.State.NULL)
        self.is_playing = False
        self.set_slider(0)


    @Gtk.Template.Callback()
    def on_volume_changed(self,widget,value):
        """Set volume from user input"""
        self.player.set_property("volume", widget.get_value()**3)

    def on_message(self, bus, message):
        """Listen to messages on the Gst bus"""
        t = message.type
        if t == Gst.MessageType.EOS:
            self.player.set_state(Gst.State.NULL)
        elif t == Gst.MessageType.ERROR:
            self.player.set_state(Gst.State.NULL)
            err, debug = message.parse_error()
            print("Error: %s" % err, debug)
        elif t == Gst.MessageType.BUFFERING:
            if message.parse_buffering() != 100:
                self.pause()
            else:
                self.play()

    @Gtk.Template.Callback()
    def on_playpause_button(self, button, data=None):
        """Handle play/pause button input"""
        if button.get_active():
            img = Gtk.Image.new_from_icon_name("media-playback-start", Gtk.IconSize.BUTTON)
            button.set_image(img)
            self.pause()
        else:
            img = Gtk.Image.new_from_icon_name("media-playback-pause", Gtk.IconSize.BUTTON)
            button.set_image(img)
            self.play()

    def update_slider(self):
        """Make the slider follow video progress"""
        if not self.is_playing:
            return False # cancel timeout
        else:
            success, new_duration = self.player.query_duration(Gst.Format.TIME)
            # adjust duration and position relative to absolute scale of 100
            #TODO actually handle failure here
            if success:
                self.duration = new_duration
            self.mult = 100 / (self.duration / Gst.SECOND)
            # fetching the position, in nanosecs
            success, new_position = self.player.query_position(Gst.Format.TIME)
            if success:
                position = new_position
            else:
                position = 0

            self.set_slider(float(position) / Gst.SECOND * self.mult)

        return True # continue calling every x milliseconds

    def set_slider(self, position):
        """Set the slider to a certain position"""
        # block seek handler so we don't seek when we set_value()
        self.progress.handler_block(self.slider_handler_id)
        self.progress.set_value(position)
        self.progress.handler_unblock(self.slider_handler_id)

    def on_slider_seek(self, widget):
        """Handle seek bar input"""
        seek_time = self.progress.get_value()
        self.player.seek_simple(Gst.Format.TIME,  Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, seek_time * Gst.SECOND / self.mult)
