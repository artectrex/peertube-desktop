#
# Copyright © 2019  Matthieu De Beule <matthieu.de@beule.be>
# Copyright © 2019  Antoine Fontaine <antoine.fontaine@epfl.ch>
#
# This file is part of PeerTube-Desktop.
#
# PeerTube-Desktop is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# PeerTube-Desktop is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# PeerTube-Desktop.  If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GLib
from gi.repository.GdkPixbuf import Pixbuf
import os
from datetime import datetime
cache_dir = os.path.join(GLib.get_user_cache_dir(), 'peertube')
if not os.path.isdir(cache_dir):
    GLib.mkdir_with_parents(cache_dir, 0o0700)

@Gtk.Template(resource_path='/org/peertube/PeerTube-Desktop/ui/video_element.ui')
class VideoElement(Gtk.EventBox):
    """A video thumbnail and associated text to be shown in a list of videos"""
    __gtype_name__ = "video_element"
    title_label = Gtk.Template.Child()
    account_label = Gtk.Template.Child()
    metadata_label = Gtk.Template.Child()
    thumbnail = Gtk.Template.Child()

    def __init__(self, window, video, session, instance_url):
        super(VideoElement, self).__init__()
        self.window = window
        self.video = video
        self.instance_url = instance_url
        self.title = video.get("name")
        self.title_label.set_property("label", self.title)

        self.account = video.get("account")
        self.account_name = self.account.get("name") + "@" + self.account.get("host")
        self.account_label.set_property("label", self.account_name)

        self.channel = video.get("channel").get("displayName")

        self.date_and_views = f"{pretty_date(video.get('publishedAt'))} - {video.get('views')} views"
        self.metadata_label.set_property("label", self.date_and_views)

        self.thumbnail_url = self.video.get("thumbnailPath")
        self.fname = os.path.join(cache_dir, self.thumbnail_url.split("/")[-1])

        thumbnail_file = Gio.File.new_for_path(self.fname)
        thumbnail_file.read_async(GLib.PRIORITY_LOW, None, self.get_thumbnail,
                                    session)

    def get_thumbnail(self, giofile, result, session):
        """Try to read thumbnail from cache or fetch it from server"""
        try:
            stream = Gio.File.read_finish(giofile, result)
            Pixbuf.new_from_stream_at_scale_async(stream, 200, 200, True, None, self.set_thumbnail, None)
        except GLib.Error as err:
            if err.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
                session.get(self.instance_url + self.thumbnail_url,
                        hooks={'response': self.save_and_set_thumbnail})
            else:
                raise

    def save_and_set_thumbnail(self, response, *args, **kwargs):
        """Save fetched image to cache and set thumbnail"""
        tmpFile = os.path.join(cache_dir, ".tmp-" + self.thumbnail_url.split("/")[-1])
        with open(tmpFile, 'wb') as thumbnail_file:
            for chunk in response.iter_content(chunk_size=128):
                thumbnail_file.write(chunk)
        thumbnail_file.close()
        os.rename(tmpFile, self.fname)
        #FIXME this isn't great, we're reading from disk instead of
        #getting it from the request directly. Too much of a hassle for now
        GLib.idle_add(self.thumbnail.set_from_pixbuf, Pixbuf.new_from_file_at_size(self.fname, 200, 200))

    def set_thumbnail(self, me, result, *data):
        """Set thumbnail from file stream"""
        try:
            self.thumbnail.set_from_pixbuf(Pixbuf.new_from_stream_finish(result))
        except GLib.Error as err:
            print(err.message)

    @Gtk.Template.Callback()
    def when_clicked(self, *args):
        """Open video if the element is clicked on"""
        self.window.scroll_stack.set_visible_child_name("loading")
        self.window.session.get(self.instance_url + "/api/v1/videos/"
                    + str(self.video.get("id")), hooks={'response': self.show_video})

    def show_video(self, response, *args, **kwargs):
        """Use response to open the video"""
        video = response.json()
        GLib.idle_add(self.window.create_watch_video, video, self.title,
                      self.date_and_views, self.channel, self.account_name)





def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    now = datetime.now()
    if isinstance(time, int):
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time, datetime):
        diff = now - time
    elif not time:
        diff = now - now
    elif isinstance(time, str):
        diff = now - datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%fZ')
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(int(second_diff)) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(int(second_diff / 60)) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(int(second_diff / 3600)) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(int(day_diff)) + " days ago"
    if day_diff < 31:
        #FIXME make it say "1 week ago" instead of weekS ago.
        #Alternatively, just find a library to do this for us, since this will
        #be a pain to translate anyway
        return str(int(day_diff / 7)) + " weeks ago"
    if day_diff < 365:
        return str(int(day_diff / 30)) + " months ago"
    return str(int(day_diff / 365)) + " years ago"
